import datetime
import logging
import logging.config
import os
import yaml


def ImportConfigFile(configFile:str) -> dict:
    """Read ConfigFile
    Parameters
    ----------
    configFile : str
      Path to and name of the config file.

    Returns
    -------
    dict
        Information from Config-File as dict.
    """
    with open(configFile, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)
        if (isinstance(cfg, dict)):
            return cfg
        else:
            raise Exception('Wrong Config-File-Format!')

def constructLoggerFromConfig(cfg: dict) -> logging.Logger :
    # read logging-configuration from config-file
    conf_file = cfg["log"]["conf_file"]   
    if os.path.exists(conf_file) == False:
        logging.basicConfig(level=logging.INFO)
    else:
        with open(conf_file, 'rt') as f:
            config = yaml.safe_load(f.read())
            logging.config.dictConfig(config)
    return logging.getLogger(cfg["log"]["logger"] ) #(__name__)
