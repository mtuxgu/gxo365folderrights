# gxo365FolderRights

Collect access-rights for a given O365 SharePoint-Folder and subfolders. The distribution of access-rights is presented in a condensed form and allows fast tracking of changes.